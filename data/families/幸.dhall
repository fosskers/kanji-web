let Entry : Type = ../types/Entry.dhall

let entry : Entry = ../functions/entry.dhall

let Tango : Type = ../types/Tango.dhall

let tango : Tango = ../functions/tango.dhall

let levels = ../types/levels.dhall

let entries
	: List Entry
	= [   entry
		⫽ { kanji =
			  "幸"
		  , oya =
			  [ "土", "干" ]
		  , onyomi =
			  [ "コウ" ]
		  , tango =
			  tango ⫽ { meishi = [ "幸せ", "幸い", "さち" ], jukugo = [ "幸福", "不幸" ] }
		  , kanken =
			  levels.hachikyuu
		  , imi =
			  [ "幸せ、運がいい", "恵み" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "報"
		  , oya =
			  [ "幸", "𠬝" ]
		  , onyomi =
			  [ "ホウ" ]
		  , tango =
				tango
			  ⫽ { doushi =
					[ "報いる", "報せる" ]
				, meishi =
					[ "報い" ]
				, jukugo =
					[ "情報", "訃報", "報道", "報酬" ]
				}
		  , kanken =
			  levels.rokkyuu
		  , imi =
			  [ "報い", "知らせ" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "執"
		  , oya =
			  [ "幸", "丸" ]
		  , onyomi =
			  [ "シツ", "シュウ" ]
		  , tango =
			  tango ⫽ { doushi = [ "執る" ], jukugo = [ "執着" ] }
		  , kanken =
			  levels.yonkyuu
		  , imi =
			  [ "手に取る", "拘る" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "摯"
		  , oya =
			  [ "執", "手" ]
		  , onyomi =
			  [ "シ" ]
		  , tango =
			  tango ⫽ { jukugo = [ "真摯" ] }
		  , kanken =
			  levels.nikyuu
		  , imi =
			  [ "まこと・真面目", "取る・持つ・掴む" ]
		  }
	  ]

in  entries
