let Entry : Type = ../types/Entry.dhall

let entry : Entry = ../functions/entry.dhall

let Tango : Type = ../types/Tango.dhall

let tango : Tango = ../functions/tango.dhall

let levels = ../types/levels.dhall

let entries
	: List Entry
	= [   entry
		⫽ { kanji =
			  "鬼"
		  , onyomi =
			  [ "キ" ]
		  , tango =
			  tango ⫽ { meishi = [ "おに" ] }
		  , kanken =
			  levels.yonkyuu
		  }
	  ,   entry
		⫽ { kanji =
			  "魂"
		  , oya =
			  [ "鬼" ]
		  , onyomi =
			  [ "コン" ]
		  , tango =
			  tango ⫽ { meishi = [ "たましい" ], jukugo = [ "魂魄" ] }
		  , kanken =
			  levels.sankyuu
		  }
	  ,   entry
		⫽ { kanji =
			  "魄"
		  , oya =
			  [ "鬼" ]
		  , onyomi =
			  [ "ハク" ]
		  , tango =
			  tango ⫽ { jukugo = [ "魂魄" ] }
		  , kanken =
			  levels.ikkyuu
		  , imi =
			  [ "人の精神や心。たましい" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "魅"
		  , oya =
			  [ "鬼", "未" ]
		  , onyomi =
			  [ "ミ" ]
		  , tango =
			  tango ⫽ { jukugo = [ "魑魅", "魅力" ] }
		  , kanken =
			  levels.sankyuu
		  , imi =
			  [ "もののけ、ばけもの", "人の心を迷わして引きつける" ]
		  }
	  ]

in  entries
