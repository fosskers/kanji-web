let Entry : Type = ../types/Entry.dhall

let entry : Entry = ../functions/entry.dhall

let Tango : Type = ../types/Tango.dhall

let tango : Tango = ../functions/tango.dhall

let levels = ../types/levels.dhall

let entries
	: List Entry
	= [ entry ⫽ { kanji = "复", onyomi = [ "フク" ] }
	  ,   entry
		⫽ { kanji =
			  "復"
		  , oya =
			  [ "复" ]
		  , onyomi =
			  [ "フク" ]
		  , tango =
			  tango ⫽ { jukugo = [ "往復", "復習", "復讐", "復活", "復興", "復路" ] }
		  , kanken =
			  levels.rokkyuu
		  , imi =
			  [ "帰る、戻る", "元に戻す", "報いる" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "履"
		  , oya =
			  [ "復" ]
		  , onyomi =
			  [ "リ" ]
		  , tango =
			  tango ⫽ { doushi = [ "履く" ], jukugo = [ "履行", "草履" ] }
		  , kanken =
			  levels.junnikyuu
		  , imi =
			  [ "履き物", "実行する" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "覆"
		  , oya =
			  [ "復" ]
		  , onyomi =
			  [ "フク" ]
		  , tango =
			  tango ⫽ { doushi = [ "覆う", "覆る", "覆す" ], jukugo = [ "転覆" ] }
		  , kanken =
			  levels.sankyuu
		  , imi =
			  [ "覆う", "覆る" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "腹"
		  , oya =
			  [ "复" ]
		  , onyomi =
			  [ "フク" ]
		  , tango =
			  tango ⫽ { meishi = [ "はら" ], jukugo = [ "空腹", "満腹", "山腹" ] }
		  , kanken =
			  levels.gokyuu
		  , imi =
			  [ "はら、おなか", "物の中央" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "複"
		  , oya =
			  [ "复" ]
		  , onyomi =
			  [ "フク" ]
		  , tango =
			  tango ⫽ { jukugo = [ "複雑" ] }
		  , kanken =
			  levels.rokkyuu
		  }
	  ,   entry
		⫽ { kanji =
			  "鰒"
		  , oya =
			  [ "复" ]
		  , onyomi =
			  [ "フク" ]
		  , tango =
			  tango ⫽ { meishi = [ "あわび", "ふぐ" ] }
		  , kanken =
			  levels.ikkyuu
		  }
	  ,   entry
		⫽ { kanji =
			  "蝮"
		  , oya =
			  [ "复" ]
		  , onyomi =
			  [ "フク" ]
		  , tango =
			  tango ⫽ { meishi = [ "まむし" ] }
		  , kanken =
			  levels.ikkyuu
		  , imi =
			  [ "まむし。有毒の蛇" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "愎"
		  , oya =
			  [ "复" ]
		  , onyomi =
			  [ "フク", "ヒョク" ]
		  , tango =
			  tango ⫽ { jukugo = [ "剛愎" ] }
		  , kanken =
			  levels.ikkyuu
		  , imi =
			  [ "もとる、そむく" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "輹"
		  , oya =
			  [ "复" ]
		  , onyomi =
			  [ "フク" ]
		  , kanken =
			  levels.ikkyuu
		  , imi =
			  [ "床縛り。車とその車軸を縛り付ける物" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "馥"
		  , oya =
			  [ "复" ]
		  , onyomi =
			  [ "フク" ]
		  , tango =
			  tango ⫽ { jukugo = [ "馥郁" ] }
		  , kanken =
			  levels.ikkyuu
		  , imi =
			  [ "香る、香り" ]
		  }
	  ]

in  entries
