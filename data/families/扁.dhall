let Entry : Type = ../types/Entry.dhall

let entry : Entry = ../functions/entry.dhall

let Tango : Type = ../types/Tango.dhall

let tango : Tango = ../functions/tango.dhall

let levels = ../types/levels.dhall

let entries
	: List Entry
	= [   entry
		⫽ { kanji =
			  "扁"
		  , onyomi =
			  [ "ヘン" ]
		  , tango =
			  tango ⫽ { keiyoushi = [ "扁たい" ], jukugo = [ "扁平" ] }
		  , kanken =
			  levels.ikkyuu
		  , imi =
			  [ "小さい", "平らで薄い。ひらたい" ]
		  }
	  , { kanji =
			"騙"
		, oya =
			[ "扁" ]
		, onyomi =
			[ "ヘン" ]
		, tango =
			tango ⫽ { doushi = [ "騙す" ] }
		, kanken =
			levels.ikkyuu
		, imi =
			[ "馬に跳び乗る", "騙す" ]
		}
	  , { kanji =
			"編"
		, oya =
			[ "扁" ]
		, onyomi =
			[ "ヘン" ]
		, tango =
			  tango
			⫽ { meishi = [ "編み笠" ], doushi = [ "編む" ], jukugo = [ "前編", "後編" ] }
		, kanken =
			levels.rokkyuu
		, imi =
			[ "編む", "書籍の中の部分け" ]
		}
	  , { kanji =
			"蝙"
		, oya =
			[ "扁" ]
		, onyomi =
			[ "ヘン" ]
		, tango =
			tango ⫽ { nandoku = [ { ji = "蝙蝠", yomi = "こうもり" } ] }
		, kanken =
			levels.ikkyuu
		, imi =
			[ "蝙蝠" ]
		}
	  , { kanji =
			"翩"
		, oya =
			[ "扁" ]
		, onyomi =
			[ "ヘン" ]
		, tango =
			tango ⫽ { jukugo = [ "翩翻" ] }
		, kanken =
			levels.ikkyuu
		, imi =
			[ "遠く飛ぶ", "ひらひらする様。翻る" ]
		}
	  ]

in  entries
