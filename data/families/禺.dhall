let Entry : Type = ../types/Entry.dhall

let entry : Entry = ../functions/entry.dhall

let Tango : Type = ../types/Tango.dhall

let tango : Tango = ../functions/tango.dhall

let levels = ../types/levels.dhall

let entries
	: List Entry
	= [ entry ⫽ { kanji = "禺", onyomi = [ "グウ" ], kanken = "一級" }
	  , { kanji =
			"遇"
		, oya =
			[ "禺" ]
		, onyomi =
			[ "グウ" ]
		, tango =
			tango ⫽ { jukugo = [ "遭遇", "処遇" ] }
		, kanken =
			levels.sankyuu
		, imi =
			[ "思いがけなく出会う", "人をもてなす" ]
		}
	  , { kanji =
			"偶"
		, oya =
			[ "禺" ]
		, onyomi =
			[ "グウ" ]
		, tango =
			  tango
			⫽ { jukugo =
				  [ "偶然" ]
			  , fukushi =
				  [ "たまたま" ]
			  , nandoku =
				  [ { ji = "木偶", yomi = "でく" } ]
			  }
		, kanken =
			levels.sankyuu
		, imi =
			[ "二つで対になる", "人形", "思いがけなく起こること。たまたま", "二つで割りきれる" ]
		}
	  , { kanji =
			"隅"
		, oya =
			[ "禺" ]
		, onyomi =
			[ "グウ" ]
		, tango =
			tango ⫽ { meishi = [ "すみ" ] }
		, kanken =
			levels.junnikyuu
		, imi =
			[ "囲まれた角の内側。すみ" ]
		}
	  , { kanji =
			"寓"
		, oya =
			[ "禺" ]
		, onyomi =
			[ "グウ" ]
		, tango =
			tango ⫽ { jukugo = [ "寓居" ] }
		, kanken =
			levels.junikkyuu
		, imi =
			[ "寄る", "一時の住まい", "自分の家をへりくだって言うこと" ]
		}
	  , { kanji =
			"愚"
		, oya =
			[ "禺" ]
		, onyomi =
			[ "グウ" ]
		, tango =
			tango ⫽ { keiyoushi = [ "愚か" ] }
		, kanken =
			levels.sankyuu
		, imi =
			[ "愚か、つまらない" ]
		}
	  ]

in  entries
