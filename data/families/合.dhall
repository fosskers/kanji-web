let Entry : Type = ../types/Entry.dhall

let entry : Entry = ../functions/entry.dhall

let Tango : Type = ../types/Tango.dhall

let tango : Tango = ../functions/tango.dhall

let levels = ../types/levels.dhall

let entries
	: List Entry
	= [   entry
		⫽ { kanji =
			  "合"
		  , onyomi =
			  [ "ゴウ" ]
		  , tango =
			  tango ⫽ { doushi = [ "合う" ], jukugo = [ "合体", "集合", "融合" ] }
		  , kanken =
			  levels.kyuukyuu
		  , imi =
			  [ "合う", "集まる" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "拾"
		  , oya =
			  [ "合" ]
		  , onyomi =
			  [ "シュウ", "ジュウ" ]
		  , tango =
			  tango ⫽ { doushi = [ "拾う" ] }
		  , kanken =
			  levels.hachikyuu
		  , imi =
			  [ "拾う", "集める", "数字の十" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "答"
		  , oya =
			  [ "合" ]
		  , onyomi =
			  [ "トウ" ]
		  , tango =
			  tango ⫽ { meishi = [ "答え" ], jukugo = [ "返答", "応答" ] }
		  , kanken =
			  levels.kyuukyuu
		  }
	  ,   entry
		⫽ { kanji =
			  "荅"
		  , oya =
			  [ "合" ]
		  , onyomi =
			  [ "トウ" ]
		  , kanken =
			  levels.ikkyuu
		  , imi =
			  [ "小豆" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "搭"
		  , oya =
			  [ "荅" ]
		  , onyomi =
			  [ "トウ" ]
		  , tango =
			  tango ⫽ { jukugo = [ "搭載", "搭乗" ] }
		  , kanken =
			  levels.junnikyuu
		  , imi =
			  [ "乗る、乗せる" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "塔"
		  , oya =
			  [ "荅" ]
		  , onyomi =
			  [ "トウ" ]
		  , kanken =
			  levels.yonkyuu
		  , imi =
			  [ "高い建物" ]
		  }
	  ]

in  entries
