let Entry : Type = ../types/Entry.dhall

let entry : Entry = ../functions/entry.dhall

let Tango : Type = ../types/Tango.dhall

let tango : Tango = ../functions/tango.dhall

let levels = ../types/levels.dhall

let entries
	: List Entry
	= [   entry
		⫽ { kanji =
			  "僉"
		  , onyomi =
			  [ "セン" ]
		  , tango =
			  tango ⫽ { meishi = [ "みな" ] }
		  , kanken =
			  levels.ikkyuu
		  , imi =
			  [ "みな" ]
		  }
	  , { kanji =
			"険"
		, oya =
			[ "僉" ]
		, onyomi =
			[ "ケン" ]
		, tango =
			tango ⫽ { jukugo = [ "危険" ], keiyoushi = [ "険しい" ] }
		, kanken =
			levels.rokkyuu
		, imi =
			[ "傾斜が急で危ない。険しい", "状態が危ない。険しい" ]
		}
	  ,   entry
		⫽ { kanji =
			  "験"
		  , oya =
			  [ "僉" ]
		  , onyomi =
			  [ "ケン", "ゲン" ]
		  , tango =
			  tango ⫽ { jukugo = [ "経験", "体験", "実験", "試験" ] }
		  , kanken =
			  levels.nanakyuu
		  , imi =
			  [ "証拠", "しるし、げん", "試す" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "剣"
		  , oya =
			  [ "僉" ]
		  , onyomi =
			  [ "ケン" ]
		  , tango =
			  tango ⫽ { meishi = [ "つるぎ", "けん" ], jukugo = [ "剣術", "剣豪" ] }
		  , kanken =
			  levels.yonkyuu
		  , imi =
			  [ "つるぎ、刀" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "検"
		  , oya =
			  [ "僉" ]
		  , onyomi =
			  [ "ケン" ]
		  , tango =
			  tango ⫽ { doushi = [ "検める" ], jukugo = [ "検定", "検査", "検索" ] }
		  , kanken =
			  levels.rokkyuu
		  , imi =
			  [ "調べる" ]
		  }
	  , { kanji =
			"瞼"
		, oya =
			[ "僉" ]
		, onyomi =
			[ "ケン" ]
		, tango =
			tango ⫽ { meishi = [ "まぶた" ], jukugo = [ "眼瞼" ] }
		, kanken =
			levels.ikkyuu
		, imi =
			[ "まぶた" ]
		}
	  , { kanji =
			"嶮"
		, oya =
			[ "僉" ]
		, onyomi =
			[ "ケン" ]
		, tango =
			tango ⫽ { jukugo = [ "嶮岨" ], keiyoushi = [ "嶮しい" ] }
		, kanken =
			levels.ikkyuu
		, imi =
			[ "「険」と同じ" ]
		}
	  ]

in  entries
