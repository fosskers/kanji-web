let Entry : Type = ../types/Entry.dhall

let entry : Entry = ../functions/entry.dhall

let Tango : Type = ../types/Tango.dhall

let tango : Tango = ../functions/tango.dhall

let levels = ../types/levels.dhall

let entries
	: List Entry
	= [   entry
		⫽ { kanji =
			  "粦"
		  , onyomi =
			  [ "リン" ]
		  , kanken =
			  levels.fumei
		  , imi =
			  [ "鬼火" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "鱗"
		  , oya =
			  [ "粦" ]
		  , onyomi =
			  [ "リン" ]
		  , tango =
			  tango ⫽ { meishi = [ "うろこ" ], jukugo = [ "片鱗" ] }
		  , kanken =
			  levels.junikkyuu
		  , imi =
			  [ "うろこ", "魚や鱗のある動物" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "麟"
		  , oya =
			  [ "粦" ]
		  , onyomi =
			  [ "リン" ]
		  , tango =
			  tango ⫽ { jukugo = [ "麒麟" ] }
		  , kanken =
			  levels.junikkyuu
		  , imi =
			  [ "麒麟" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "隣"
		  , oya =
			  [ "粦" ]
		  , onyomi =
			  [ "リン" ]
		  , tango =
			  tango ⫽ { meishi = [ "隣り" ], jukugo = [ "隣接", "隣国", "隣家" ] }
		  , kanken =
			  levels.yonkyuu
		  , imi =
			  [ "隣り" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "燐"
		  , oya =
			  [ "粦" ]
		  , onyomi =
			  [ "リン" ]
		  , tango =
				tango
			  ⫽ { meishi =
					[ "リン" ]
				, jukugo =
					[ "燐火" ]
				, nandoku =
					[ { ji = "燐寸", yomi = "マッチ" } ]
				}
		  , kanken =
			  levels.junikkyuu
		  , imi =
			  [ "元素のリン", "鬼火" ]
		  }
	  ,   entry
		⫽ { kanji =
			  "鄰"
		  , oya =
			  [ "粦" ]
		  , onyomi =
			  [ "リン" ]
		  , kanken =
			  levels.junikkyuu
		  }
	  ,   entry
		⫽ { kanji =
			  "憐"
		  , oya =
			  [ "粦" ]
		  , onyomi =
			  [ "レン" ]
		  , tango =
			  tango ⫽ { jukugo = [ "哀憐" ] }
		  , kanken =
			  levels.junikkyuu
		  , imi =
			  [ "哀れみ", "愛しく思う" ]
		  }
	  ]

in  entries
