let Tango : Type = ../types/Tango.dhall

let tango : Tango = ./tango.dhall

let levels = ../types/levels.dhall

let Entry : Type = ../types/Entry.dhall

let entry
	: Entry
	= { kanji =
		  ""
	  , oya =
		  [] : List Text
	  , onyomi =
		  [] : List Text
	  , tango =
		  tango
	  , kanken =
		  levels.fumei
	  , imi =
		  [] : List Text
	  }

in  entry