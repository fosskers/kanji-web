let NanDoku : Type = ../types/NanDoku.dhall

let Tango : Type = ../types/Tango.dhall

let tango
	: Tango
	= { doushi =
		  [] : List Text
	  , meishi =
		  [] : List Text
	  , keiyoushi =
		  [] : List Text
	  , fukushi =
		  [] : List Text
	  , joshi =
		  [] : List Text
	  , jukugo =
		  [] : List Text
	  , yojijukugo =
		  [] : List Text
	  , nandoku =
		  [] : List NanDoku
	  }

in  tango
