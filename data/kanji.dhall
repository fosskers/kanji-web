let NanDoku : Type = ./types/NanDoku.dhall

let Tango : Type = ./types/Tango.dhall

let tango : Tango = ./functions/tango.dhall

let Entry : Type = ./types/Entry.dhall

let levels = ./types/levels.dhall

let entry : Entry = ./functions/entry.dhall

let entries
	: List (List Entry)
	= [ ./families/"禺.dhall"
	  , ./families/"扁.dhall"
	  , ./families/"合.dhall"
	  , ./families/"僉.dhall"
	  , ./families/"粦.dhall"
	  , ./families/"复.dhall"
	  ]

in  entries
