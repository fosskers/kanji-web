let levels =
	  { juukyuu =
		  "十級"
	  , kyuukyuu =
		  "九級"
	  , hachikyuu =
		  "八級"
	  , nanakyuu =
		  "七級"
	  , rokkyuu =
		  "六級"
	  , gokyuu =
		  "五級"
	  , yonkyuu =
		  "四級"
	  , sankyuu =
		  "三級"
	  , junnikyuu =
		  "準二級"
	  , nikyuu =
		  "二級"
	  , junikkyuu =
		  "準一級"
	  , ikkyuu =
		  "一級"
	  , fumei =
		  "不明"
	  }

in  levels