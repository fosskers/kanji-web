let Tango : Type = ./Tango.dhall

let Entry
	: Type
	= { kanji :
		  Text
	  , oya :
		  List Text
	  , onyomi :
		  List Text
	  , tango :
		  Tango
	  , kanken :
		  Text
	  , imi :
		  List Text
	  }

in  Entry