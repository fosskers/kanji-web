let NanDoku : Type = ./NanDoku.dhall

let Tango
	: Type
	= { doushi :
		  List Text
	  , meishi :
		  List Text
	  , keiyoushi :
		  List Text
	  , fukushi :
		  List Text
	  , joshi :
		  List Text
	  , jukugo :
		  List Text
	  , yojijukugo :
		  List Text
	  , nandoku :
		  List NanDoku
	  }

in  Tango