{-# LANGUAGE LambdaCase       #-}
{-# LANGUAGE TypeApplications #-}

module Main where

-- import qualified Data.ByteString.Lazy.Char8 as B
-- import           Data.Kanji.Web
import Test.Tasty
-- import           Test.Tasty.HUnit

---

main :: IO ()
main = defaultMain suite

suite :: TestTree
suite = testGroup "Unit Tests"
  [
  ]
