{-# LANGUAGE OverloadedStrings #-}

-- |
-- Module    : Data.Kanji.Web
-- Copyright : (c) Colin Woodbury, 2019
-- License   : BSD3
-- Maintainer: Colin Woodbury <colin@fosskers.ca>
--
module Data.Kanji.Web where

import qualified Data.Kanji.Types as K
import           Data.List.NonEmpty (NonEmpty)
import           Data.Text (Text)

---

-- TODO Once `haskell-src-exts` can parse UTF8 function names, switch all these
-- field names to use Kanji.

-- | 漢字情報。「`親`」によって漢字網が成り立つ。
data Entry = Entry
  { kanji  :: K.Kanji
    -- ^ 漢和辞書でも見つかる、漢字の部首。
  , oya    :: [K.Kanji]
    -- ^ 部首意外の漢字の形や読みの由来。無い可能性もあるため、型を List にしてある。
  , onyomi :: NonEmpty Text
    -- ^ 旧中国から渡来した読み。
  , tango  :: Vocab
    -- ^ その他の読み、熟語、四字熟語など。
  , kanken :: Text  -- TODO Make this `Level`
    -- ^ 当てはまる漢字検定の級。
  , imi    :: NonEmpty Text
    -- ^ 日本語に関係する字の主な意味。
  }

-- | ただ「訓読み」と載せるよりも、読みとその具体的な種類。
data Vocab = Vocab
  { doushi     :: [Text]
  , meishi     :: [Text]
  , keiyoushi  :: [Text]
  , fukushi    :: [Text]
  , joshi      :: [Text]
  , jukugo     :: [Text]
  , yojijukugo :: [Text]
  , nandoku    :: [Yomi] }

-- | ある単語とその読み。
data Yomi = Yomi { ji :: Text, yomi :: Text }
